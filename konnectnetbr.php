<?php
/**
 * Konnect - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   Konnect
 * @author    WeDo Digital <contato@wedodigital.com.br>
 * @copyright 2021 WeDo Digital
 * @license   Proprietary https://wedodigital.com.br
 * @link      https://wedodigital.com.br
 *
 * @wordpress-plugin
 * Plugin Name: Konnect - mu-plugin
 * Plugin URI:  https://wedodigital.com.br
 * Description: Customizations for konnect.net.br site
 * Version:     1.0.2
 * Author:      WeDo Digital
 * Author URI:  https://wedodigital.com.br/
 * Text Domain: konnect
 * License:     Proprietary
 * License URI: https://wedodigital.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('konnectnetbr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Hide editor
 */

add_action(
    'admin_head',
    function () {
        $template_file = $template_file = basename(get_page_template());

        $templates = array("page-politica-de-privacidade.blade.php");
        if (!in_array($template_file, $templates)) { 
            remove_post_type_support('page', 'editor');
        }
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

/**********
 * Show on Front Page
 *
 * @return bool $display
 */
function Show_On_Front_page($cmb)
{
    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option('page_on_front');

    // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($front_page) : $front_page;

    // There is a front page set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**********
 * Show on Privacy Policy 
 *
 * @return bool $display
 */
function Show_On_privacy($cmb)
{
    // Get ID of page set as privacy, 0 if there isn't one
    $privacy = get_option('wp_page_for_privacy_policy');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($privacy) : $privacy;

    // There is a privacy set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**
 * Show on Show_On_Front_page
 *
 * @return bool $display
 *
 * @author Tom Morton
 * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
 */
function Show_On_plan()
{
    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return false;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    $slugs = array("combos", "internet", "tv"); 

    $parents = get_post_ancestors($post_id);

    return in_array($slug, $slugs) AND (!$parents);
}

 /**
  * Metabox for Page Slug
  *
  * @param bool  $display  Display
  * @param array $meta_box Metabox 
  *
  * @return bool $display
  *
  * @author Tom Morton
  * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
  */
/*
function Cmb2_Metabox_Show_On_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    $parent_slug = true;
    if (isset($meta_box['show_on']['parant_slug'])) {
        $parents = get_post_ancestors($post_id); 
        $id = ($parents) ? $parents[count($parents)-1]: false;
        if ($id) {
            $parent = get_post($id);
            $parent_slug = $parent->post_name == $meta_box['show_on']['parent_slug'];
        }
    }

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']) AND $parent_slug;
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_slug', 10, 2);
*/

/**
 * Metabox for Children of Parent ID
 *
 * @param bool  $display
 * @param array $meta_box
 *
 * @return bool display metabox
 * 
 * @author Kenneth White (GitHub: sprclldr)
 * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
 */
/*
function Cmb2_Metabox_Show_On_Child_of($display, $meta_box) 
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('child_of' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (! $post_id) {
        return $display;
    }

    $pageids = array();
    foreach ((array) $meta_box['show_on']['value'] as $parent_id) {
        $pages = get_pages(
            array(
                'child_of'    => $parent_id,
                'post_status' => 'publish,draft,pending',
            )
        );

        if ($pages) {
            foreach ($pages as $page) {
                $pageids[] = $page->ID;
            }
        }
    }
    $pageids_unique = array_unique($pageids);

    return in_array($post_id, $pageids_unique);
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_Child_of', 10, 2);
/*

/**
 * Removes metabox from appearing on post new screens before the post
 * ID has been set.
 *
 * @param bool  $display
 * @param array $meta_box The array of metabox options
 * 
 * @return bool $display True on success, false on failure
 * 
 * @author Thomas Griffin
 */
/*
function Cmb2_Exclude_From_New($display, $meta_box)
{
    if (!isset($meta_box['show_on']['alt_key'], $meta_box['show_on']['alt_value'])) {
        return $display;
    }

    if ('exclude_new' !== $meta_box['show_on']['alt_key']) {
        return $display;
    }

    global $pagenow;

    // Force to be an array
    $to_exclude = !is_array($meta_box['show_on']['alt_value'])
        ? array( $meta_box['show_on']['alt_value'] )
        : $meta_box['show_on']['alt_value'];

    $is_new_post = 'post-new.php' == $pagenow && in_array('post-new.php', $to_exclude);

    return ! $is_new_post;
}
add_filter('cmb2_show_on', 'Cmb2_Exclude_From_New', 10, 2);
*/

/**
 * Exclude metabox on non top level posts
 *
 * @param bool $display
 * @param array $meta_box
 * 
 * @return bool display metabox
 * 
 * @author Travis Northcutt
 * @link   https://gist.github.com/gists/2039760
 */
/*
function Cmb2_Metabox_Add_For_Top_Level_Posts_only( $display, $meta_box )
{
    if (!isset($meta_box['show_on']['top_key']) || 'parent-id' !== $meta_box['show_on']['top_key']) {
        return $display;
    }

    $post_id = 0;
    
    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif ( isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }
    
    if (!$post_id) {
        return $display;
    }
    
    // If the post doesn't have ancestors, show the box
    if (!get_post_ancestors($post_id)) {
        return $display;
    }
    
    // Otherwise, it's not a top level post, so don't show it
    return false;
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Add_For_Top_Level_Posts_only', 10, 2);
*/

/**
 * Gets a number of terms and displays them as options
 *
 * @param CMB2_Field $field CMB2 Field
 *
 * @return array An array of options that matches the CMB2 options array
 */
function Cmb2_getTermOptions($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/***********************************************************************************
 * Register post types
 * ********************************************************************************/

/**
 * Combos
 **/
function cpt_konnectnetbr_combos() 
{
    $labels = array(
            'name'                  => __('Combos', 'konnectnetbr'),
            'singular_name'         => __('Combo', 'konnectnetbr'),
            'menu_name'             => __('Combos', 'konnectnetbr'),
            'name_admin_bar'        => __('Combo', 'konnectnetbr'),
            'archives'              => __('Combo Archives', 'konnectnetbr'),
            'attributes'            => __('Combo Attributes', 'konnectnetbr'),
            'parent_item_colon'     => __('Parent Combo:', 'konnectnetbr'),
            'all_items'             => __('All Combos', 'konnectnetbr'),
            'add_new_item'          => __('Add New Combo', 'konnectnetbr'),
            'add_new'               => __('Add new _combo', 'konnectnetbr'),
            'new_item'              => __('New Combo', 'konnectnetbr'),
            'edit_item'             => __('Edit Combo', 'konnectnetbr'),
            'update_item'           => __('Update Combo', 'konnectnetbr'),
            'view_item'             => __('View Combo', 'konnectnetbr'),
            'view_items'            => __('View Combos', 'konnectnetbr'),
            'search_items'          => __('Search Combo', 'konnectnetbr'),
            'not_found'             => __('Not found', 'konnectnetbr'),
            'not_found_in_trash'    => __('Not found in Trash', 'konnectnetbr'),
            'featured_image'        => __('Featured Image', 'konnectnetbr'),
            'set_featured_image'    => __('Set featured image', 'konnectnetbr'),
            'remove_featured_image' => __('Remove featured image', 'konnectnetbr'),
            'use_featured_image'    => __('Use as featured image', 'konnectnetbr'),
            'insert_into_item'      => __('Insert into combo', 'konnectnetbr'),
            'uploaded_to_this_item' => __('Uploaded to this combo', 'konnectnetbr'),
            'items_list'            => __('Combos list', 'konnectnetbr'),
            'items_list_navigation' => __('Combos list navigation', 'konnectnetbr'),
            'filter_items_list'     => __('Filter Combos list', 'konnectnetbr'),
    );
    $rewrite = array(
            'slug'                  => __('combos', 'konnectnetbr'),
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Combos', 'konnectnetbr'),
            'description'           => __('Konnect Combos', 'konnectnetbr'),
            'labels'                => $labels,
            'supports'              => array('title', 'revisions', 'page-attributes'),
            'taxonomies'            => array(''),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.5 13.5C2.5 13.3674 2.55268 13.2402 2.64645 13.1464C2.74021 13.0527 2.86739 13 3 13H13C13.1326 13 13.2598 13.0527 13.3536 13.1464C13.4473 13.2402 13.5 13.3674 13.5 13.5C13.5 13.6326 13.4473 13.7598 13.3536 13.8536C13.2598 13.9473 13.1326 14 13 14H3C2.86739 14 2.74021 13.9473 2.64645 13.8536C2.55268 13.7598 2.5 13.6326 2.5 13.5ZM13.991 3L14.015 3.001C14.2018 3.01372 14.3845 3.06227 14.553 3.144C14.6744 3.20048 14.7786 3.28812 14.855 3.398C14.922 3.498 15 3.675 15 4V9.991L14.999 10.015C14.9862 10.2018 14.9376 10.3845 14.856 10.553C14.7995 10.6743 14.7118 10.7785 14.602 10.855C14.502 10.922 14.325 11 14 11H2.009L1.985 10.999C1.79817 10.9862 1.61554 10.9376 1.447 10.856C1.32567 10.7995 1.22148 10.7118 1.145 10.602C1.078 10.502 1 10.325 1 10V4.009L1.001 3.985C1.01372 3.79815 1.06227 3.6155 1.144 3.447C1.20052 3.32567 1.28816 3.22148 1.398 3.145C1.498 3.078 1.675 3 2 3H13.991ZM14 2H2C0 2 0 4 0 4V10C0 12 2 12 2 12H14C16 12 16 10 16 10V4C16 2 14 2 14 2Z"/><path d="M12.5754 6.23714C12.7644 6.04814 12.7451 5.73139 12.5202 5.58526C11.1748 4.71135 9.60432 4.24744 7.99998 4.25001C6.33311 4.25001 4.77998 4.74001 3.47973 5.58526C3.42771 5.62037 3.38422 5.66668 3.35246 5.7208C3.32069 5.77492 3.30146 5.83548 3.29617 5.89801C3.29088 5.96054 3.29968 6.02346 3.3219 6.08215C3.34413 6.14083 3.37922 6.19379 3.4246 6.23714C3.49916 6.3099 3.59623 6.35516 3.69989 6.3655C3.80356 6.37583 3.90765 6.35062 3.9951 6.29401C5.19038 5.52844 6.58055 5.12265 7.99998 5.12501C9.41941 5.12265 10.8096 5.52844 12.0049 6.29401C12.1842 6.40951 12.4249 6.38851 12.5754 6.23714ZM10.6652 8.14726C10.863 7.94951 10.8271 7.61789 10.5777 7.49101C9.77929 7.0847 8.89586 6.87359 7.99998 6.87501C7.07248 6.87501 6.19573 7.09726 5.42223 7.49101C5.17285 7.61789 5.13698 7.94951 5.33473 8.14726L5.34785 8.16039C5.48785 8.30039 5.70485 8.32664 5.88248 8.23914C6.54155 7.91612 7.26601 7.74878 7.99998 7.75001C8.75948 7.75001 9.47873 7.92589 10.1175 8.24001C10.2951 8.32751 10.5112 8.30126 10.6521 8.16039L10.6652 8.14726V8.14726ZM8.92748 9.88501C9.09898 9.71351 9.10073 9.43001 8.89248 9.30751C8.62253 9.14683 8.31413 9.06217 7.99998 9.06251C7.68583 9.06217 7.37743 9.14683 7.10748 9.30751C6.89923 9.43001 6.90098 9.71351 7.07248 9.88501L7.69023 10.5028C7.73087 10.5435 7.77915 10.5758 7.8323 10.5979C7.88545 10.6199 7.94243 10.6313 7.99998 10.6313C8.05753 10.6313 8.11451 10.6199 8.16766 10.5979C8.22081 10.5758 8.26909 10.5435 8.30973 10.5028L8.92836 9.88414L8.92748 9.88501Z"/></svg>'),
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('konnect_combos', $args); 
} 
add_action('init', 'cpt_konnectnetbr_combos', 0);

/**
 * Internet Plans
 */
function cpt_konnectnetbr_internet() 
{
    $labels = array(
        'name'                  => __('Internet Plans', 'konnectnetbr'),
        'singular_name'         => __('Internet Plan', 'konnectnetbr'),
        'menu_name'             => __('Internet Plans', 'konnectnetbr'),
        'name_admin_bar'        => __('Internet Plan', 'konnectnetbr'),
        'archives'              => __('Internet Plan Archives', 'konnectnetbr'),
        'attributes'            => __('Internet Plan Attributes', 'konnectnetbr'),
        'parent_item_colon'     => __('Parent Internet Plan:', 'konnectnetbr'),
        'all_items'             => __('All Internet Plans', 'konnectnetbr'),
        'add_new_item'          => __('Add New Internet Plan', 'konnectnetbr'),
        'add_new'               => __('Add new _internet plan', 'konnectnetbr'),
        'new_item'              => __('New Internet Plan', 'konnectnetbr'),
        'edit_item'             => __('Edit Internet Plan', 'konnectnetbr'),
        'update_item'           => __('Update Internet Plan', 'konnectnetbr'),
        'view_item'             => __('View Internet Plan', 'konnectnetbr'),
        'view_items'            => __('View Internet Plans', 'konnectnetbr'),
        'search_items'          => __('Search Internet Plan', 'konnectnetbr'),
        'not_found'             => __('Not found', 'konnectnetbr'),
        'not_found_in_trash'    => __('Not found in Trash', 'konnectnetbr'),
        'featured_image'        => __('Featured Image', 'konnectnetbr'),
        'set_featured_image'    => __('Set featured image', 'konnectnetbr'),
        'remove_featured_image' => __('Remove featured image', 'konnectnetbr'),
        'use_featured_image'    => __('Use as featured image', 'konnectnetbr'),
        'insert_into_item'      => __('Insert into internet plan', 'konnectnetbr'),
        'uploaded_to_this_item' => __('Uploaded to this internet plan', 'konnectnetbr'),
        'items_list'            => __('Internet Plans list', 'konnectnetbr'),
        'items_list_navigation' => __('Internet Plans list navigation', 'konnectnetbr'),
        'filter_items_list'     => __('Filter Internet Plans list', 'konnectnetbr'),
    );
    $rewrite = array(
            'slug'                  => __('internet', 'konnectnetbr'),
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Internet Plans', 'konnectnetbr'),
            'description'           => __('Konnect Internet Plans', 'konnectnetbr'),
            'labels'                => $labels,
            'supports'              => array('title', 'revisions', 'page-attributes'),
            'taxonomies'            => array(''),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" fill="none" class="bi bi-wifi" viewBox="0 0 16 16"><path d="M15.385 6.115a.485.485 0 0 0-.048-.736A12.443 12.443 0 0 0 8 3 12.44 12.44 0 0 0 .663 5.379a.485.485 0 0 0-.048.736.518.518 0 0 0 .668.05A11.448 11.448 0 0 1 8 4c2.507 0 4.827.802 6.717 2.164.204.148.489.13.668-.049z"/><path d="M13.229 8.271c.216-.216.194-.578-.063-.745A9.456 9.456 0 0 0 8 6c-1.905 0-3.68.56-5.166 1.526a.48.48 0 0 0-.063.745.525.525 0 0 0 .652.065A8.46 8.46 0 0 1 8 7a8.46 8.46 0 0 1 4.577 1.336c.205.132.48.108.652-.065zm-2.183 2.183c.226-.226.185-.605-.1-.75A6.472 6.472 0 0 0 8 9c-1.06 0-2.062.254-2.946.704-.285.145-.326.524-.1.75l.015.015c.16.16.408.19.611.09A5.478 5.478 0 0 1 8 10c.868 0 1.69.201 2.42.56.203.1.45.07.611-.091l.015-.015zM9.06 12.44c.196-.196.198-.52-.04-.66A1.99 1.99 0 0 0 8 11.5a1.99 1.99 0 0 0-1.02.28c-.238.14-.236.464-.04.66l.706.706a.5.5 0 0 0 .708 0l.707-.707z"/></svg>'),
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('konnect_internet', $args); 
} 
add_action('init', 'cpt_konnectnetbr_internet', 0);

/**
 * TV Plans
 */

function cpt_konnectnetbr_tv() 
{
    /*
    $labels = array(
        'name'                  => __('TV Plans', 'konnectnetbr'),
        'singular_name'         => __('TV Plan', 'konnectnetbr'),
        'menu_name'             => __('TV Plans', 'konnectnetbr'),
        'name_admin_bar'        => __('TV Plan', 'konnectnetbr'),
        'archives'              => __('TV Plan Archives', 'konnectnetbr'),
        'attributes'            => __('TV Plan Attributes', 'konnectnetbr'),
        'parent_item_colon'     => __('Parent TV Plan:', 'konnectnetbr'),
        'all_items'             => __('All TV Plans', 'konnectnetbr'),
        'add_new_item'          => __('Add New TV Plan', 'konnectnetbr'),
        'add_new'               => __('Add new _tv plan', 'konnectnetbr'),
        'new_item'              => __('New TV Plan', 'konnectnetbr'),
        'edit_item'             => __('Edit TV Plan', 'konnectnetbr'),
        'update_item'           => __('Update TV Plan', 'konnectnetbr'),
        'view_item'             => __('View TV Plan', 'konnectnetbr'),
        'view_items'            => __('View TV Plans', 'konnectnetbr'),
        'search_items'          => __('Search TV Plan', 'konnectnetbr'),
        'not_found'             => __('Not found', 'konnectnetbr'),
        'not_found_in_trash'    => __('Not found in Trash', 'konnectnetbr'),
        'featured_image'        => __('Featured Image', 'konnectnetbr'),
        'set_featured_image'    => __('Set featured image', 'konnectnetbr'),
        'remove_featured_image' => __('Remove featured image', 'konnectnetbr'),
        'use_featured_image'    => __('Use as featured image', 'konnectnetbr'),
        'insert_into_item'      => __('Insert into tv plan', 'konnectnetbr'),
        'uploaded_to_this_item' => __('Uploaded to this tv plan', 'konnectnetbr'),
        'items_list'            => __('TV Plans list', 'konnectnetbr'),
        'items_list_navigation' => __('TV Plans list navigation', 'konnectnetbr'),
        'filter_items_list'     => __('Filter TV Plans list', 'konnectnetbr'),
    );
    $rewrite = array(
            'slug'                  => __('tv', 'konnectnetbr'),
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('TV Plans', 'konnectnetbr'),
            'description'           => __('Konnect TV Plans', 'konnectnetbr'),
            'labels'                => $labels,
            'supports'              => array('title', 'revisions', 'page-attributes'),
            'taxonomies'            => array(''),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-tv" viewBox="0 0 16 16"><path d="M2.5 13.5A.5.5 0 0 1 3 13h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zM13.991 3l.024.001a1.46 1.46 0 0 1 .538.143.757.757 0 0 1 .302.254c.067.1.145.277.145.602v5.991l-.001.024a1.464 1.464 0 0 1-.143.538.758.758 0 0 1-.254.302c-.1.067-.277.145-.602.145H2.009l-.024-.001a1.464 1.464 0 0 1-.538-.143.758.758 0 0 1-.302-.254C1.078 10.502 1 10.325 1 10V4.009l.001-.024a1.46 1.46 0 0 1 .143-.538.758.758 0 0 1 .254-.302C1.498 3.078 1.675 3 2 3h11.991zM14 2H2C0 2 0 4 0 4v6c0 2 2 2 2 2h12c2 0 2-2 2-2V4c0-2-2-2-2-2z"/></svg>'),
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('konnect_tv', $args); 
    */
    
    //Unregister post type 'konnect_tv' if exist
    if (post_type_exists('konnect_tv')) {
        unregister_post_type('konnect_tv');
    }
} 
add_action('init', 'cpt_konnectnetbr_tv', 0);

/***********************************************************************************
 * CPT Fields
 * ********************************************************************************/

/**
 * Combos CTP Fields
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_combo_cpt_';

        /******
         * Settings
         ******/
        $cmb_combo_settings = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_combo_cpt_settings_id',
                'title'         => __('Plan settings', 'konnectnetbr'),
                'object_types'  => array('konnect_combos'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_combo_settings->add_field(
            array(
                'name'       => __('Display on front page', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_show_on_front',
                'type'       => 'checkbox',
            )
        );

        //Featured
        $cmb_combo_settings->add_field(
            array(
                'name'       => __('Featured plan', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_featured',
                'type'       => 'checkbox',
            )
        );

        //Label
        $cmb_combo_settings->add_field(
            array(
                'name'       => __('Label text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_label_text',
                'type'       => 'text',
            )
        );

        //Title
        $cmb_combo_settings->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_combo_settings->add_field(
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_subtitle',
                'type'       => 'text',
            )
        );

        //Label Description
        $cmb_combo_settings->add_field(
            array(
                'name'       => __('Description', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_desc',
                'type'       => 'text',
            )
        );

        //Price Prefix
        //$cmb_combo_settings->add_field(
        //    array(
        //        'name'       => __('Price - Prefix', 'konnectnetbr'),
        //        'desc'       => '',
        //        'id'         => $prefix . 'settings_price_prefix',
        //        'type'       => 'text',
        //        'default'    => __('From', 'konnectnetbr'),
        //    )
        //);

        //Price
        $cmb_combo_settings->add_field(
            array(
                'name'       => __('Price - Unit', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_price_unit',
                'type'       => 'text',
            )
        );

        //Price Cents
        $cmb_combo_settings->add_field(
            array(
                'name'       => __('Price - Cents', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_price_cents',
                'type'       => 'text',
                'default'    => '00',
            )
        );

        //Price Period
        $cmb_combo_settings->add_field(
            array(
                'name'       => __('Price - Period', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_price_period',
                'type'       => 'text',
                'default'    => __('month', 'konnectnetbr'),
            )
        );

        //Price Suffix
        $cmb_combo_settings->add_field(
            array(
                'name'       => __('Price - Suffix', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_price_suffix',
                'type'       => 'text',
            )
        );

        //Btn Text
        $cmb_combo_settings->add_field(
            array(
                'name'       => __('Button Text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_btn_text',
                'type'       => 'text',
                'default'    => __('Hire now', 'konnectnetbr'),
            )
        );

        /******
         * Internet
         ******/
        $cmb_combo_internet = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_combo_cpt_internet_id',
                'title'         => __('Internet', 'konnectnetbr'),
                'object_types'  => array('konnect_combos'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Plan Internet Speed
        $cmb_combo_internet->add_field(
            array(
                'name'       => __('Download Speed', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'internet_down_speed',
                'type'       => 'text_small',
            )
        );

        //Plan Internet Items
        $cmb_combo_internet->add_field(
            array(
                'name'       => __('Items', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'internet_items',
                'type'       => 'textarea_code',
            )
        );

        /******
         * TV
         ******/
        $cmb_combo_tv = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_combo_cpt_tv_id',
                'title'         => __('TV', 'konnectnetbr'),
                'object_types'  => array('konnect_combos'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Plan TV channels
        $cmb_combo_tv->add_field(
            array(
                'name'       => __('Number of channels', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'tv_num_channels',
                'type'       => 'text_small',
            )
        );

        //Plan TV Items
        $cmb_combo_tv->add_field(
            array(
                'name'       => __('Items', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'tv_items',
                'type'       => 'textarea_code',
            )
        );

        /******
         * General Conditions
         ******/
        $cmb_combo_conditions = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_combo_cpt_conditions_id',
                'title'         => __('General Conditions', 'konnectnetbr'),
                'object_types'  => array('konnect_combos'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Plan Conditions Items
        $cmb_combo_conditions->add_field(
            array(
                'name'       => __('Items', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'conditions_items',
                'type'       => 'textarea_code',
            )
        );
    }
);

/**
 * Internet CPT Fields
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_internet_cpt_';

        /******
         * Settings
         ******/
        $cmb_internet_settings = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_internet_cpt_settings_id',
                'title'         => __('Plan settings', 'konnectnetbr'),
                'object_types'  => array('konnect_internet'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_internet_settings->add_field(
            array(
                'name'       => __('Display on front page', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_show_on_front',
                'type'       => 'checkbox',
            )
        );

        //Featured
        $cmb_internet_settings->add_field(
            array(
                'name'       => __('Featured plan', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_featured',
                'type'       => 'checkbox',
            )
        );

        //Label
        $cmb_internet_settings->add_field(
            array(
                'name'       => __('Label text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_label_text',
                'type'       => 'text',
            )
        );

        //Title
        $cmb_internet_settings->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_internet_settings->add_field(
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_subtitle',
                'type'       => 'text',
            )
        );

        //Label Description
        $cmb_internet_settings->add_field(
            array(
                'name'       => __('Description', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_desc',
                'type'       => 'text',
            )
        );

        //Price Prefix
        //$cmb_internet_settings->add_field(
        //    array(
        //        'name'       => __('Price - Prefix', 'konnectnetbr'),
        //        'desc'       => '',
        //        'id'         => $prefix . 'settings_price_prefix',
        //        'type'       => 'text',
        //        'default'    => __('From', 'konnectnetbr'),
        //    )
        //);

        //Price
        $cmb_internet_settings->add_field(
            array(
                'name'       => __('Price - Unit', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_price_unit',
                'type'       => 'text',
            )
        );

        //Price Cents
        $cmb_internet_settings->add_field(
            array(
                'name'       => __('Price - Cents', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_price_cents',
                'type'       => 'text',
                'default'    => '00',
            )
        );

        //Price Period
        $cmb_internet_settings->add_field(
            array(
                'name'       => __('Price - Period', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_price_period',
                'type'       => 'text',
                'default'    => __('month', 'konnectnetbr'),
            )
        );

        //Price Suffix
        $cmb_internet_settings->add_field(
            array(
                'name'       => __('Price - Suffix', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_price_suffix',
                'type'       => 'text',
            )
        );

        //Btn Text
        $cmb_internet_settings->add_field(
            array(
                'name'       => __('Button Text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_btn_text',
                'type'       => 'text',
                'default'    => __('Hire now', 'konnectnetbr'),
            )
        );

        /******
         * Internet
         ******/
        $cmb_internet_internet = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_internet_cpt_internet_id',
                'title'         => __('Internet', 'konnectnetbr'),
                'object_types'  => array('konnect_internet'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Plan Internet Speed
        $cmb_internet_internet->add_field(
            array(
                'name'       => __('Download Speed', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'internet_down_speed',
                'type'       => 'text_small',
            )
        );

        //Plan Internet Items
        $cmb_internet_internet->add_field(
            array(
                'name'       => __('Items', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'internet_items',
                'type'       => 'textarea_code',
            )
        );

        /******
         * General Conditions
         ******/
        $cmb_internet_conditions = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_internet_cpt_conditions_id',
                'title'         => __('General Conditions', 'konnectnetbr'),
                'object_types'  => array('konnect_internet'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Plan Conditions Items
        $cmb_internet_conditions->add_field(
            array(
                'name'       => __('Items', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'conditions_items',
                'type'       => 'textarea_code',
            )
        );
    }
);

/**
 * TV CPT Fields
 */
/*
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_tv_cpt_';

        //******
        //* Settings
        //******
        $cmb_tv_settings = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_tv_cpt_settings_id',
                'title'         => __('Plan settings', 'konnectnetbr'),
                'object_types'  => array('konnect_tv'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_tv_settings->add_field(
            array(
                'name'       => __('Display on front page', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_show_on_front',
                'type'       => 'checkbox',
            )
        );

        //Featured
        $cmb_tv_settings->add_field(
            array(
                'name'       => __('Featured plan', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_featured',
                'type'       => 'checkbox',
            )
        );

        //Label
        $cmb_tv_settings->add_field(
            array(
                'name'       => __('Label text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_label_text',
                'type'       => 'text',
            )
        );

        //Title
        $cmb_tv_settings->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_tv_settings->add_field(
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_subtitle',
                'type'       => 'text',
            )
        );

        //Label Description
        $cmb_tv_settings->add_field(
            array(
                'name'       => __('Description', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_desc',
                'type'       => 'text',
            )
        );

        //Price Prefix
        //$cmb_tv_settings->add_field(
        //    array(
        //        'name'       => __('Price - Prefix', 'konnectnetbr'),
        //        'desc'       => '',
        //        'id'         => $prefix . 'settings_price_prefix',
        //        'type'       => 'text',
        //        'default'    => __('From', 'konnectnetbr'),
        //    )
        //);

        //Price
        $cmb_tv_settings->add_field(
            array(
                'name'       => __('Price - Unit', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_price_unit',
                'type'       => 'text',
            )
        );

        //Price Cents
        $cmb_tv_settings->add_field(
            array(
                'name'       => __('Price - Cents', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_price_cents',
                'type'       => 'text',
                'default'    => '00',
            )
        );

        //Price Period
        $cmb_tv_settings->add_field(
            array(
                'name'       => __('Price - Period', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_price_period',
                'type'       => 'text',
                'default'    => __('month', 'konnectnetbr'),
            )
        );

        //Price Suffix
        $cmb_tv_settings->add_field(
            array(
                'name'       => __('Price - Suffix', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_price_suffix',
                'type'       => 'text',
            )
        );

        //Btn Text
        $cmb_tv_settings->add_field(
            array(
                'name'       => __('Button Text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'settings_btn_text',
                'type'       => 'text',
                'default'    => __('Hire now', 'konnectnetbr'),
            )
        );

        //******
        //* TV
        //******
        $cmb_tv_tv = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_tv_cpt_tv_id',
                'title'         => __('TV', 'konnectnetbr'),
                'object_types'  => array('konnect_tv'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Plan TV channels
        $cmb_tv_tv->add_field(
            array(
                'name'       => __('Number of channels', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'tv_num_channels',
                'type'       => 'text_small',
            )
        );

        //Plan TV Items
        $cmb_tv_tv->add_field(
            array(
                'name'       => __('Items', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'tv_items',
                'type'       => 'textarea_code',
            )
        );

        //******
        //* General Conditions
        //******
        $cmb_tv_conditions = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_tv_cpt_conditions_id',
                'title'         => __('General Conditions', 'konnectnetbr'),
                'object_types'  => array('konnect_tv'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'combos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Plan Conditions Items
        $cmb_tv_conditions->add_field(
            array(
                'name'       => __('Items', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'conditions_items',
                'type'       => 'textarea_code',
            )
        );
    }
);
*/

/***********************************************************************************
 * Page Fields
 * ********************************************************************************/

/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_frontpage_';

        /******
         * Menu
         ******/
        $cmb_menu = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_frontpage_menu_id',
                'title'         => __('Menu', 'konnectnetbr'),
                'object_types'  => array('page'), // post type
                'show_names'   => true,
                'show_on_cb' => 'Show_On_Front_page',
                //'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Title
        $cmb_menu->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'menu_title',
                'type'       => 'text',
            )
        );

        //Sel service Group
        $menu_id = $cmb_menu->add_field(
            array(
                'id'          => $prefix . 'menu_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Menu Item {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Menu Item', 'konnectnetbr'),
                    'remove_button' =>__('Remove Menu Item', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Menu Icon
        $cmb_menu->add_group_field(
            $menu_id,
            array(
                'name'       => __('Icon', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'icon',
                'type'       => 'textarea_code',
            )
        );

        //Menu Title
        $cmb_menu->add_group_field(
            $menu_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Menu URL
        $cmb_menu->add_group_field(
            $menu_id,
            array(
                'name'       => __('URL', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'link',
                'type'       => 'text',
            )
        );
    }
);

/**
 * For You
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_foryou_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_foryou_hero_id',
                'title'         => __('Hero', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-para-voce.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                'desc'       => 'Cores: Branco = white, Preto = black, Azul Claro = #04c0ff, Azul Padrão = #002b85, Azul Escuro = #00236b, Azul Mais Escuro = #001542, Amarelo Padrão = #fdd100, Amarelo Escuro = #f7b400',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );


        //Hero Image Inner (Promo)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Promo)', 'konnectnetbr'),
                'description' => __('PNG with transparent background', 'konnectnetbr'),
                'id'          => 'img_inner',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'konnectnetbr'),
                'desc'       => '',
                'default'    => __('Hire now', 'konnectnetbr'),
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'konnectnetbr'),
                'desc'       => 'https://konnect.net.br/...',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );

        //Hero Button CSS classes
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button CSS Classes', 'konnectnetbr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white, wedo-c-button--black', 'konnectnetbr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary',
                'type'       => 'text',
            )
        );

        /******
         * Offers
         ******/
        $cmb_offers = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_foryou_offers_id',
                'title'         => __('Offers', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-para-voce.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_offers->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'offers_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );

        //Title
        $cmb_offers->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'offers_title',
                'type'       => 'text',
            )
        );

        //Offers Subtitle
        /*
        $cmb_offers->add_field(
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'offers_subtitle',
                'type'       => 'text',
            )
        );
        */

        /******
         * Self service
         ******/
        $cmb_selfservice = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_foryou_selfservice_id',
                'title'         => __('Self Service', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-para-voce.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_selfservice->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'selfservice_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );

        //Title
        $cmb_selfservice->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'selfservice_title',
                'type'       => 'text',
            )
        );

        //Sel service Group
        $selfservice_id = $cmb_selfservice->add_field(
            array(
                'id'          => $prefix . 'selfservice_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Self service {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Self service', 'konnectnetbr'),
                    'remove_button' =>__('Remove Self service', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Self service Icon
        //$cmb_selfservice->add_group_field(
        //    $selfservice_id,
        //    array(
        //        'name'        => __('Icon', 'konnectnetbr'),
        //        'description' => '',
        //        'id'          => 'icon',
        //        'type'        => 'file',
        //        // Optional:
        //        'options' => array(
        //            'url' => false, // Hide the text input for the url
        //        ),
        //        'text'    => array(
        //            'add_upload_file_text' =>__('Add Icon', 'konnectnetbr'),
        //        ),
        //        // query_args are passed to wp.media's library query.
        //        'query_args' => array(
        //            'type' => array(
        //                //'image/gif',
        //                //'image/jpeg',
        //                //'image/png',
        //                'image/svg+xml'
        //            ),
        //        ),
        //        'preview_size' => array(48, 48)
        //    )
        //);

        //Self service Icon
        $cmb_selfservice->add_group_field(
            $selfservice_id,
            array(
                'name'       => __('Self service', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'icon',
                'type'       => 'textarea_code',
            )
        );

        //Self service Title
        $cmb_selfservice->add_group_field(
            $selfservice_id,
            array(
                'name'       => __('Self service', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Self service URL
        $cmb_selfservice->add_group_field(
            $selfservice_id,
            array(
                'name'       => __('Self service URL', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'link',
                'type'       => 'text',
            )
        );

        /**
         * CTA
         */
        $cmb_cta = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_foryou_cta_id',
                'title'         => __('CTA', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-para-voce.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_cta->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'cta_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );

        //CTA Group
        $cta_id = $cmb_cta->add_field(
            array(
                'id'          => $prefix . 'cta_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //CTA Background Color
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //CTA Background Image (Desktop)
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //CTA Background Image (Mobile)
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //CTA Image
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'        => __('Image', 'konnectnetbr'),
                'description' => '',
                'id'          => 'img',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //CTA Title
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );

        //CTA text
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'text',
                'type'       => 'textarea',
            )
        );

        //CTA Button Text
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Button Text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //CTA Button URL
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Button URL', 'konnectnetbr'),
                'desc'       => 'https://konnect.net.br/...',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );

        //CTA Button CSS classes
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Button CSS Classes', 'konnectnetbr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white, wedo-c-button--black', 'konnectnetbr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary',
                'type'       => 'text',
            )
        );

        /**
         * Advantages
         */
        $cmb_advantages = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_foryou_advantages_id',
                'title'         => __('Advantages', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-para-voce.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_advantages->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'advantages_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );

        //Title
        $cmb_advantages->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'advantages_title',
                'type'       => 'text',
            )
        );

        //Advantages Group
        $advantages_id = $cmb_advantages->add_field(
            array(
                'id'          => $prefix . 'advantages_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Advantage {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Advantage', 'konnectnetbr'),
                    'remove_button' =>__('Remove Advantage', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Advantage Icon
        //$cmb_advantages->add_group_field(
        //    $advantages_id,
        //    array(
        //        'name'        => __('Icon', 'konnectnetbr'),
        //        'description' => '',
        //        'id'          => 'icon',
        //        'type'        => 'file',
        //        // Optional:
        //        'options' => array(
        //            'url' => false, // Hide the text input for the url
        //        ),
        //        'text'    => array(
        //            'add_upload_file_text' =>__('Add Icon', 'konnectnetbr'),
        //        ),
        //        // query_args are passed to wp.media's library query.
        //        'query_args' => array(
        //            'type' => array(
        //                //'image/gif',
        //                //'image/jpeg',
        //                //'image/png',
        //                'image/svg+xml'
        //            ),
        //        ),
        //        'preview_size' => array(48, 48)
        //    )
        //);
        
        //Advantage Title
        $cmb_advantages->add_group_field(
            $advantages_id,
            array(
                'name'       => __('Advantage', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'icon',
                'type'       => 'textarea_code',
            )
        );

        //Advantage Title
        $cmb_advantages->add_group_field(
            $advantages_id,
            array(
                'name'       => __('Advantage', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text_medium',
            )
        );
    }
);

/**
 * Combo / Internet / TV
 */    
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_plan_';

        /**
         * Hero
         */

        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_plan_hero_id',
                'title'         => __('Hero', 'konnectnetbr'),
                'object_types'  => array('page'), // post type
                'show_names'    => true,
                'show_on_cb'    => 'Show_On_plan',
                //'show_on'       => array('key' => 'slug', 'value' => array('combos', 'internet')),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
        
        //Show
        /*
        $cmb_hero->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );
        */

        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => 'Cores: Branco = white, Preto = black, Azul Claro = #04c0ff, Azul Padrão = #002b85, Azul Escuro = #00236b, Azul Mais Escuro = #001542, Amarelo Padrão = #fdd100, Amarelo Escuro = #f7b400',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        /*
        //Hero Image Inner (Promo)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Image Inner (Promo)', 'konnectnetbr'),
                'description' => __('PNG with transparent background', 'konnectnetbr'),
                'id'          => 'img_inner',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'konnectnetbr'),
                'desc'       => '',
                'default'    => __('Hire now', 'konnectnetbr'),
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'konnectnetbr'),
                'desc'       => 'https://konnect.net.br/...',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );

        //Hero Button CSS classes
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button CSS Classes', 'konnectnetbr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white, wedo-c-button--black', 'konnectnetbr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary',
                'type'       => 'text',
            )
        );
        */

        /******
         * Plans
         ******/
        $cmb_plans = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_plan_plans_id',
                'title'         => __('Plans', 'konnectnetbr'),
                'object_types'  => array('page'), // post type
                'show_names'    => true,
                'show_on_cb'    => 'Show_On_plan',
                //'show_on'       => array('key' => 'slug', 'value' => array('combos', 'internet')),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        /*
        //Show
        $cmb_plans->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'offers_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );
        */

        //Title
        $cmb_plans->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'text',
            )
        );

        //Plan Subtitle
        $cmb_plans->add_field(
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'text',
            )
        );
        
        /**
         * Advantages
         */
        $cmb_advantages = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_plan_advantages_id',
                'title'         => __('Advantages', 'konnectnetbr'),
                'object_types'  => array('page'), // post type
                'show_names'   => true,
                'show_on_cb'    => 'Show_On_plan',
                //'show_on'       => array('key' => 'slug', 'value' => array('combos', 'internet')),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_advantages->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'advantages_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );

        /**
         * CTA
         */
        $cmb_cta = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_plan_cta_id',
                'title'         => __('CTA', 'konnectnetbr'),
                'object_types'  => array('page'), // post type
                'show_names'   => true,
                'show_on_cb'    => 'Show_On_plan',
                //'show_on'       => array('key' => 'slug', 'value' => array('combos', 'internet')),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_cta->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'cta_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );

        //CTA Group
        $cta_id = $cmb_cta->add_field(
            array(
                'id'          => $prefix . 'cta_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //CTA Background Color
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //CTA Background Image (Desktop)
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //CTA Background Image (Mobile)
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //CTA Image
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'        => __('Image', 'konnectnetbr'),
                'description' => '',
                'id'          => 'img',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //CTA Title
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );

        //CTA text
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'text',
                'type'       => 'textarea',
            )
        );

        //CTA Button Text
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Button Text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //CTA Button URL
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Button URL', 'konnectnetbr'),
                'desc'       => 'https://konnect.net.br/...',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );

        //CTA Button CSS classes
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Button CSS Classes', 'konnectnetbr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white, wedo-c-button--black', 'konnectnetbr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary',
                'type'       => 'text',
            )
        );

        /******
         * Self service
         ******/
        $cmb_selfservice = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_plans_selfservice_id',
                'title'         => __('Self Service', 'konnectnetbr'),
                'object_types'  => array('page'), // post type
                'show_names'   => true,
                'show_on_cb'    => 'Show_On_plan',
                //'show_on'       => array('key' => 'slug', 'value' => array('combos', 'internet')),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_selfservice->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'selfservice_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );
    }
);

/**
 * Attendance
 */    
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_attendance_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_attendance_hero_id',
                'title'         => __('Hero', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-atendimento.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => 'Cores: Branco = white, Preto = black, Azul Claro = #04c0ff, Azul Padrão = #002b85, Azul Escuro = #00236b, Azul Mais Escuro = #001542, Amarelo Padrão = #fdd100, Amarelo Escuro = #f7b400',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Channels
         ******/
        $cmb_channels = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_attendance_channels_id',
                'title'         => __('Service channels', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-atendimento.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_channels->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'channels_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );

        //Title
        $cmb_channels->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'channels_title',
                'type'       => 'text',
            )
        );

        //Form shortcode 
        $cmb_channels->add_field(
            array(
                'name'       => __('Form Shortcode', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'channels_form_shortcode',
                'type'       => 'text',
            )
        );

        //Phones
        $phones_id = $cmb_channels->add_field(
            array(
                'id'          => $prefix . 'channels_phones',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Phone {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Phone', 'konnectnetbr'),
                    'remove_button' =>__('Remove Phone', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        $cmb_channels->add_group_field(
            $phones_id,
            array(
                'name'       => __('URL', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'url',
                'type'       => 'text',
            )
        );
        
        $cmb_channels->add_group_field(
            $phones_id,
            array(
                'name'       => __('Navigation label', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'label',
                'type'       => 'text',
            )
        );

        $cmb_channels->add_group_field(
            $phones_id,
            array(
                'name'       => __('Title attribute', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );

        //WhatsApp 
        $whatsapps_id = $cmb_channels->add_field(
            array(
                'id'          => $prefix . 'channels_whatsapps',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('WhatsApp {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another WhatsApp', 'konnectnetbr'),
                    'remove_button' =>__('Remove WhatsApp', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        $cmb_channels->add_group_field(
            $whatsapps_id,
            array(
                'name'       => __('URL', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'url',
                'type'       => 'text',
            )
        );
        
        $cmb_channels->add_group_field(
            $whatsapps_id,
            array(
                'name'       => __('Navigation label', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'label',
                'type'       => 'text',
            )
        );

        $cmb_channels->add_group_field(
            $whatsapps_id,
            array(
                'name'       => __('Title attribute', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );

        //Notes
        $cmb_channels->add_field(
            array(
                'name'       => __('Notes', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'channels_notes',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Self service
         ******/
        $cmb_selfservice = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_attendance_selfservice_id',
                'title'         => __('Self Service', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-atendimento.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_selfservice->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'selfservice_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );

        /**
         * Advantages
         */
        $cmb_advantages = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_attendance_advantages_id',
                'title'         => __('Advantages', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-atendimento.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_advantages->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'advantages_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );
    }
);

/**
 * Por que Konnect
 */    
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_konnect_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_konnect_hero_id',
                'title'         => __('Hero', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-por-que-konnect.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => 'Cores: Branco = white, Preto = black, Azul Claro = #04c0ff, Azul Padrão = #002b85, Azul Escuro = #00236b, Azul Mais Escuro = #001542, Amarelo Padrão = #fdd100, Amarelo Escuro = #f7b400',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );


        /******
         * About
        ********/
        $cmb_about = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_konnect_about_id',
                'title'         => __('About', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-por-que-konnect.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Title
        $cmb_about->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'about_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_about->add_field(
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'about_subtitle',
                'type'       => 'text',
            )
        );

        //Text 
        $cmb_about->add_field(
            array(
                'name'       => __('Text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'about_text',
                'type'       => 'textarea_code',
            )
        );

        /**
         * CTA
         */
        $cmb_cta = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_konnect_cta_id',
                'title'         => __('CTA', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-por-que-konnect.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_cta->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'cta_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );

        //CTA Group
        $cta_id = $cmb_cta->add_field(
            array(
                'id'          => $prefix . 'cta_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //CTA Background Color
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //CTA Background Image (Desktop)
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //CTA Background Image (Mobile)
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //CTA Image
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'        => __('Image', 'konnectnetbr'),
                'description' => '',
                'id'          => 'img',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //CTA Title
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text',
            )
        );

        //CTA text
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'text',
                'type'       => 'textarea',
            )
        );

        //CTA Button Text
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Button Text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_medium',
            )
        );

        //CTA Button URL
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Button URL', 'konnectnetbr'),
                'desc'       => 'https://konnect.net.br/...',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );

        //CTA Button CSS classes
        $cmb_cta->add_group_field(
            $cta_id,
            array(
                'name'       => __('Button CSS Classes', 'konnectnetbr'),
                'desc'       => __('Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white, wedo-c-button--black', 'konnectnetbr'),
                'id'         => 'btn_css',
                'default'    => 'wedo-c-button--secondary',
                'type'       => 'text',
            )
        );

        /**
         * Advantages
         */
        $cmb_advantages = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_konnect_advantages_id',
                'title'         => __('Advantages', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-por-que-konnect.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_advantages->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'advantages_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );

        /******
         * Self service
         ******/
        $cmb_selfservice = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_konnect_selfservice_id',
                'title'         => __('Self Service', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-por-que-konnect.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_selfservice->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'selfservice_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );
    }
);

/**
 * Help
 */    
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_help_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_help_hero_id',
                'title'         => __('Hero', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-ajuda.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => 'Cores: Branco = white, Preto = black, Azul Claro = #04c0ff, Azul Padrão = #002b85, Azul Escuro = #00236b, Azul Mais Escuro = #001542, Amarelo Padrão = #fdd100, Amarelo Escuro = #f7b400',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Questions
         ******/
        $cmb_questions = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_help_questions_id',
                'title'         => __('Common Questions', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-ajuda.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Title
        $cmb_questions->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'questions_title',
                'type'       => 'text',
            )
        );

        //Questions Group
        $questions_id = $cmb_questions->add_field(
            array(
                'id'          => $prefix . 'questions_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Question {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Question', 'konnectnetbr'),
                    'remove_button' =>__('Remove Question', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Question
        $cmb_questions->add_group_field(
            $questions_id,
            array(
                'name'       => __('Question', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'question',
                'type'       => 'text',
            )
        );

        //Answer
        $cmb_questions->add_group_field(
            $questions_id,
            array(
                'name'       => __('Answer', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'answer',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Self service
         ******/
        $cmb_selfservice = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_help_selfservice_id',
                'title'         => __('Self Service', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-ajuda.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_selfservice->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'selfservice_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );

        /**
         * Advantages
         */
        $cmb_advantages = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_help_advantages_id',
                'title'         => __('Advantages', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-ajuda.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show
        $cmb_advantages->add_field(
            array(
                'name'       => __('Show', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'advantages_show',
                'type'       => 'checkbox',
                //'default'    => 'true'
            )
        );
    }
);

/**
 * My Konnect
 */    
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_minhakonnect_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_minhakonnect_hero_id',
                'title'         => __('Hero', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-minha-konnect.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => 'Cores: Branco = white, Preto = black, Azul Claro = #04c0ff, Azul Padrão = #002b85, Azul Escuro = #00236b, Azul Mais Escuro = #001542, Amarelo Padrão = #fdd100, Amarelo Escuro = #f7b400',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Registration
        ********/
        $cmb_registration = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_minhakonnect_registration_id',
                'title'         => __('Registration', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-minha-konnect.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Registration Title
        $cmb_registration->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'registration_title',
                'type'       => 'text',
            )
        );

        //Registration Subtitle
        $cmb_registration->add_field(
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'registration_subtitle',
                'type'       => 'text',
            )
        );

        //Registration Icon
        $cmb_registration->add_field(
            array(
                'name'       => __('Icon', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'registration_item_icon',
                'type'       => 'textarea_code',
            )
        );

        //Registration Title
        $cmb_registration->add_field(
            array(
                'name'       => __('Button text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'registration_item_title',
                'type'       => 'text_medium',
            )
        );

        //Registration Page slug
        $cmb_registration->add_field(
            array(
                'name'        => __('Page slug', 'konnectnetbr'),
                'description' => '',
                'id'          => $prefix . 'registration_item_page_slug',
                'type'       => 'text_small',
            )
        );

        /******
         * 2 Way of Accounts
        ********/
        $cmb_twowaysofaccount = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_minhakonnect_2wayofaccount_id',
                'title'         => __('2 Way of Accounts', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-minha-konnect.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //2 Way of Accounts Title
        $cmb_twowaysofaccount->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . '2wayofaccount_title',
                'type'       => 'text',
            )
        );

        //2 Way of Accounts Subtitle
        $cmb_twowaysofaccount->add_field(
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . '2wayofaccount_subtitle',
                'type'       => 'text',
            )
        );

        //2 Way of Accounts Group
        $twowaysofaccount_id = $cmb_twowaysofaccount->add_field(
            array(
                'id'          => $prefix . '2wayofaccount_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Account {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Account', 'konnectnetbr'),
                    'remove_button' =>__('Remove Account', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //2 Way of Accounts Icon
        $cmb_twowaysofaccount->add_group_field(
            $twowaysofaccount_id,
            array(
                'name'       => __('Icon', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'icon',
                'type'       => 'textarea_code',
            )
        );

        //2 Way of Accounts Title
        $cmb_twowaysofaccount->add_group_field(
            $twowaysofaccount_id,
            array(
                'name'       => __('Account', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text_medium',
            )
        );

        //2 Way of Accounts Page slug
        $cmb_twowaysofaccount->add_group_field(
            $twowaysofaccount_id,
            array(
                'name'        => __('Page slug', 'konnectnetbr'),
                'description' => '',
                'id'          => 'page_slug',
                'type'       => 'text_small',
            )
        );

        /******
         * Contracts
        ********/
        $cmb_contracts = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_minhakonnect_contracts_id',
                'title'         => __('Contracts', 'konnectnetbr'),
                'object_types'  => array('page'), // post type
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-minha-konnect.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Title
        $cmb_contracts->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'contracts_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_contracts->add_field(
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'contracts_subtitle',
                'type'       => 'text',
            )
        );

        //Contracts Group
        $contracts_id = $cmb_contracts->add_field(
            array(
                'id'          => $prefix . 'contracts_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Contract {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Contract', 'konnectnetbr'),
                    'remove_button' =>__('Remove Contract', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Contract Icon
        $cmb_contracts->add_group_field(
            $contracts_id,
            array(
                'name'       => __('Icon', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'icon',
                'type'       => 'textarea_code',
            )
        );

        //Contract Title
        $cmb_contracts->add_group_field(
            $contracts_id,
            array(
                'name'       => __('Contract', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text_medium',
            )
        );

        //Contract Page slug
        $cmb_contracts->add_group_field(
            $contracts_id,
            array(
                'name'        => __('Page slug', 'konnectnetbr'),
                'description' => '',
                'id'          => 'page_slug',
                'type'       => 'text_small',
            )
        );
    }
);

/**
 * My Konnect / Registration
 */    
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_registration_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_registration_hero_id',
                'title'         => __('Hero', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-cadastro.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => 'Cores: Branco = white, Preto = black, Azul Claro = #04c0ff, Azul Padrão = #002b85, Azul Escuro = #00236b, Azul Mais Escuro = #001542, Amarelo Padrão = #fdd100, Amarelo Escuro = #f7b400',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Content
        ********/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_registration_content_id',
                'title'         => __('Content', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-cadastro.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'text',
            )
        );

        //Text 
        $cmb_content->add_field(
            array(
                'name'       => __('Form My Konnect', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'content_text',
                'type'       => 'text',
            )
        );

    }
);

/**
 * Contracts
 */    
/*
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_contracts_';

        //**
        // Hero
        //**
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_contracts_hero_id',
                'title'         => __('Hero', 'konnectnetbr'),
                'object_types'  => array('page'), // post type
                'show_names'   => true,
                //'show_on_cb' => 'Show_On_Front_page',
                'show_on' => array('key' => 'slug', 'value' => 'contratos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => 'Cores: Branco = white, Preto = black, Azul Claro = #04c0ff, Azul Padrão = #002b85, Azul Escuro = #00236b, Azul Mais Escuro = #001542, Amarelo Padrão = #fdd100, Amarelo Escuro = #f7b400',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //******
        / * Contracts
        //********
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_contracts_content_id',
                'title'         => __('Content', 'konnectnetbr'),
                'object_types'  => array('page'), // post type
                'show_names'   => true,
                //'show_on_cb' => 'Show_On_Front_page',
                'show_on' => array('key' => 'slug', 'value' => 'contratos'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'text',
            )
        );

        //Text 
        $cmb_content->add_field(
            array(
                'name'       => __('Text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'content_text',
                'type'       => 'text',
            )
        );

        //Contracts Group
        $contracts_id = $cmb_content->add_field(
            array(
                'id'          => $prefix . 'contracts_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Contract {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Contract', 'konnectnetbr'),
                    'remove_button' =>__('Remove Contract', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Contract Icon
        $cmb_content->add_group_field(
            $contracts_id,
            array(
                'name'       => __('Icon', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'icon',
                'type'       => 'textarea_code',
            )
        );

        //Contract Title
        $cmb_content->add_group_field(
            $contracts_id,
            array(
                'name'       => __('Contract', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text_medium',
            )
        );

        //Contract Page slug
        $cmb_content->add_group_field(
            $contracts_id,
            array(
                'name'        => __('Page slug', 'konnectnetbr'),
                'description' => '',
                'id'          => 'page_slug',
                'type'       => 'text_small',
            )
        );

        
    }
);
*/

/**
 * Contracts Child
 */    
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_contracts-child_';

        /******
         * 2 Way of Accounts
        ********/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_contracts-child_content_id',
                'title'         => __('Content', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/template-contratos.blade.php'),
                //'show_on'       => array('key' => 'child_of', 'value' => '137'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Contracts File
        $cmb_content->add_field(
            array(
                'name'        => __('PDF File', 'konnectnetbr'),
                'description' => '',
                'id'          => $prefix.'pdf_file',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add PDF file', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => 'application/pdf'
                ),
                'preview_size' => array(210, 297)
            )
        );
    }
);

/**
 * 2 Way of Accounts
 */    
/*
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_2wayofaccount_';

        //**
        // * Hero
        // *
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_2wayofaccount_hero_id',
                'title'         => __('Hero', 'konnectnetbr'),
                'object_types'  => array('page'), // post type
                'show_names'   => true,
                //'show_on_cb' => 'Show_On_Front_page',
                'show_on' => array('key' => 'slug', 'value' => '2-via-conta'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => 'Cores: Branco = white, Preto = black, Azul Claro = #04c0ff, Azul Padrão = #002b85, Azul Escuro = #00236b, Azul Mais Escuro = #001542, Amarelo Padrão = #fdd100, Amarelo Escuro = #f7b400',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //******
        // * 2 Way of Accounts
        ********
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_2wayofaccount_content_id',
                'title'         => __('Content', 'konnectnetbr'),
                'object_types'  => array('page'), // post type
                'show_names'   => true,
                //'show_on_cb' => 'Show_On_Front_page',
                'show_on' => array('key' => 'slug', 'value' => '2-via-conta'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'text',
            )
        );

        //Text 
        $cmb_content->add_field(
            array(
                'name'       => __('Text', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'content_text',
                'type'       => 'text',
            )
        );

        //2 Way of Accounts Group
        $twowaysofaccount_id = $cmb_content->add_field(
            array(
                'id'          => $prefix . '2wayofaccount_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Account {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Account', 'konnectnetbr'),
                    'remove_button' =>__('Remove Account', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //2 Way of Accounts Icon
        $cmb_content->add_group_field(
            $twowaysofaccount_id,
            array(
                'name'       => __('Icon', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'icon',
                'type'       => 'textarea_code',
            )
        );

        //2 Way of Accounts Title
        $cmb_content->add_group_field(
            $twowaysofaccount_id,
            array(
                'name'       => __('Account', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'text_medium',
            )
        );

        //2 Way of Accounts Link
        $cmb_content->add_group_field(
            $twowaysofaccount_id,
            array(
                'name'        => __('Site URL', 'konnectnetbr'),
                'description' => '',
                'id'          => 'site_url',
                'type'       => 'text_url',
            )
        );

        //2 Way of Accounts Page slug
        $cmb_content->add_group_field(
            $twowaysofaccount_id,
            array(
                'name'        => __('Page slug', 'konnectnetbr'),
                'description' => '',
                'id'          => 'page_slug',
                'type'       => 'text_small',
            )
        );
    }
);
*/

/**
 * 2 Way of Accounts Child
 */    
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_2wayofaccount-child_';

        /******
         * 2 Way of Accounts
        ********/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_2wayofaccount-child_content_id',
                'title'         => __('Content', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/template-central-do-assinante.blade.php'),
                //'show_on'       => array('key' => 'child_of', 'value' => '146'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //2 Way of Accounts Link
        $cmb_content->add_field(
            array(
                'name'        => __('Site URL', 'konnectnetbr'),
                'description' => '',
                'id'          => $prefix.'site_url',
                'type'       => 'text_url',
            )
        );
    }
);



/**
 * Companies
 */    
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_konnectnetbr_companies_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_companies_hero_id',
                'title'         => __('Hero', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-para-empresas.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'konnectnetbr'),
                    'add_button'   =>__('Add Another Slide', 'konnectnetbr'),
                    'remove_button' =>__('Remove Slide', 'konnectnetbr'),
                    'sortable'      => true,
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Background Image (Desktop)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Landscape Background Image (Desktop)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Background Image (Mobile)
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Portrait Background Image (Mobile)', 'konnectnetbr'),
                'description' => '',
                'id'          => 'bkg_img_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'konnectnetbr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Subtitle
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle',
                'type'       => 'text_medium',
            )
        );

        //Hero Subtitle Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Subtitle Color', 'konnectnetbr'),
                'desc'       => '',
                'id'         => 'subtitle_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#dfefff', '#04c0ff', '#002b85', '#00236b', '#001542', '#fffedf', '#fdd100', '#f7b400'),
                        ),
                    ),
                ),
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => 'Cores: Branco = white, Preto = black, Azul Claro = #04c0ff, Azul Padrão = #002b85, Azul Escuro = #00236b, Azul Mais Escuro = #001542, Amarelo Padrão = #fdd100, Amarelo Escuro = #f7b400',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Contact
        ********/
        $cmb_contact = new_cmb2_box(
            array(
                'id'            => 'konnectnetbr_companies_contact_id',
                'title'         => __('Contact', 'konnectnetbr'),
                'show_names'   => true,
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'views/page-para-empresas.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Title
        $cmb_contact->add_field(
            array(
                'name'       => __('Title', 'konnectnetbr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_title',
                'type'       => 'text',
            )
        );

        //Form Shortcode
        $cmb_contact->add_field(
            array(
                'name'       => __('Contact Form Shortcode', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_form_shortcode',
                'type'       => 'text',
            )
        );        
    }
);
